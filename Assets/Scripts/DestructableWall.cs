﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestructableWall : MonoBehaviour
{
	private SpriteRenderer spriteRenderer;
	private BoxCollider2D boxCollider2D;

	private void Start()
	{
		spriteRenderer = GetComponent<SpriteRenderer>();
		boxCollider2D = GetComponent<BoxCollider2D>();
	}

	// This is purposely very basic at the moment. In the future will be expanded with better visual effects
	public void Destroy()
	{
		spriteRenderer.enabled = false;
		boxCollider2D.enabled = false;
	}
}
