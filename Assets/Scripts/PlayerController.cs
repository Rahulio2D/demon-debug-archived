﻿using System.Collections;
using UnityEngine;

public class PlayerController : Character
{

	[Header("Horizontal Movement")]
	public float maxRunSpeed;
	public float groundedControl;
	public float inAirControl;
	private float moveSpeed;
	private bool playerCanControl;
	private int directionFaced;

	[Header("Jumping")]
	public float jumpVelocity;
	public float maxCoyoteTime;				// Time in which player can jump after leaving ground/platform
	private float coyoteTimeRemaining;

	[Header("Wall Jumping")]
	public Vector2 wallJumpDistance;  // Jump on the same wall
	public float wallJumpTime;
	public Vector2 wallLeapDistance;  // Jump to a different wall
	public float wallLeapTime;
	public Vector2 wallHopDistance;   // Jump off wall
	public float wallHopTime;
	public float wallSlideSpeed;      // Slide down speed when on wall

	[Header("Attack")]
	public float attackSpeed;         // Horizontal speed of attack
	public float attackBuildUpTime;
	public float attackTime;
	public float attackCooldown;			// Time after attack before player can move
	private bool isAttacking;

	[Header("Input")]
	public string horizontalAxis;
	public string verticalAxis;
	public KeyCode[] jumpButtons;
	public KeyCode[] attackButtons;
	private Vector2 previousFrameInputDirection;

	public static PlayerController Instance;
	new void Awake()
	{
		if (Instance == null) Instance = this;
		else Destroy(this.gameObject);

		base.Awake();
		moveSpeed = 0;
		coyoteTimeRemaining = 0;
		playerCanControl = true;
		isAttacking = false;
		directionFaced = 1;
	}

	new void Update()
	{
		base.Update();

		Vector2 inputDirection = new Vector2(Input.GetAxisRaw(horizontalAxis), Input.GetAxisRaw(verticalAxis));
		if (velocity.x != 0) directionFaced = velocity.x > 0 ? 1 : -1;

		if (playerCanControl)
		{
			MoveHorizontal(inputDirection.x);
			if (CanJump() && PressedJumpButton()) Jump();
			if (IsOnWall()) WallJump(inputDirection.x);
			if (CanAttack() && PressedAttackButton()) StartCoroutine(Attack());
		}

		CheckCollisions();

		coyoteTimeRemaining = IsGrounded() ? maxCoyoteTime : coyoteTimeRemaining -= Time.deltaTime;
		previousFrameInputDirection = inputDirection;
	}

	new void FixedUpdate()
	{
		// Only update gravity if not on the wall and player has control
		SetGravityEnabled(!IsOnWall() && playerCanControl);
		base.FixedUpdate();
	}

	private void MoveHorizontal(float inputDirectionX)
	{
		float targetMoveSpeed = maxRunSpeed * inputDirectionX;
		float lerpSpeed = IsGrounded() ? groundedControl : inAirControl;
		moveSpeed = Mathf.MoveTowards(moveSpeed, targetMoveSpeed, lerpSpeed * Time.deltaTime);
		velocity.x = moveSpeed;
	}

	private void Jump()
	{
		velocity.y = jumpVelocity;
	}

	private IEnumerator Attack()
	{
		isAttacking = true;
		playerCanControl = false;
		velocity.x = 0;
		yield return new WaitForSeconds(attackBuildUpTime);
		float attackTimeRemaining = attackTime;
		while(attackTimeRemaining > 0)
		{
			velocity.x = attackSpeed * directionFaced;
			attackTimeRemaining -= Time.deltaTime;
			yield return null;
		};
		velocity.x = 0;
		yield return new WaitForSeconds(attackCooldown);
		isAttacking = false;
		playerCanControl = true;
	}

	private void WallJump(float inputDirectionX)
	{
		velocity.y = -wallSlideSpeed;

		if (PressedJumpButton())
		{
			if(inputDirectionX == 0) StartCoroutine(PerformWallAction(wallHopDistance, wallHopTime));
			else if(inputDirectionX == physicsSystem.GetDirection().x) StartCoroutine(PerformWallAction(wallJumpDistance, wallJumpTime));
			else StartCoroutine(PerformWallAction(wallLeapDistance, wallLeapTime, true));
		}
	}

	private IEnumerator PerformWallAction(Vector2 wallActionVelocity, float wallActionTime, bool continueVelocity = false)
	{
		playerCanControl = false;
		velocity.x = wallActionVelocity.x * physicsSystem.GetDirection().x * -1;
		velocity.y = wallActionVelocity.y;
		yield return new WaitForSeconds(wallActionTime);
		if(continueVelocity) moveSpeed = velocity.x;
		playerCanControl = true;
	}

	private void CheckCollisions()
	{
		if(isAttacking)
		{
			if(physicsSystem.CheckCollision("Destructable Wall"))
			{
				DestructableWall destructableWall = physicsSystem.GetCollisionObjectsByTag("Destructable Wall")[0].GetComponent<DestructableWall>();
				if (destructableWall) destructableWall.Destroy();
			}
			if (physicsSystem.CheckCollision("Enemy"))
			{
				StartCoroutine(EnemyHitEffect());
				EnemyController enemyController = physicsSystem.GetCollisionObjectsByTag("Enemy")[0].GetComponent<EnemyController>();
				enemyController.Launch(position);
			}
		}
	}

	private IEnumerator EnemyHitEffect()
	{
		Time.timeScale = 0;
		yield return new WaitForSecondsRealtime(0.02f);
		Time.timeScale = 1;
	}

	private bool PressedJumpButton()
	{
		foreach (KeyCode key in jumpButtons)
		{
			if (Input.GetKeyDown(key)) return true;
		}
		return false;
	}

	private bool PressedAttackButton()
	{
		foreach (KeyCode key in attackButtons)
		{
			if (Input.GetKeyDown(key)) return true;
		}
		return false;
	}

	private bool CanJump()
	{
		return IsGrounded() || coyoteTimeRemaining >= 0;
	}

	private bool IsOnWall()
	{
		return !IsGrounded() && GetHorizontalCollision() && velocity.y <= 0;
	}

	private bool CanAttack()
	{
		return IsGrounded() && !isAttacking;
	}

	private new bool GetHorizontalCollision()
	{
		if (physicsSystem.collision.right && moveSpeed > 0f) return true;
		if (physicsSystem.collision.left && moveSpeed < 0f) return true;
		return false;
	}
}
