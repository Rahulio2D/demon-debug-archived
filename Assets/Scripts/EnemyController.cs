﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : Character
{
	public Vector2 launchVelocity;

	public void Launch(Vector2 playerPosition)
	{
		float directionX = ((Vector2)transform.position - playerPosition).normalized.x;
		velocity.x = launchVelocity.x * directionX;
		velocity.y = launchVelocity.y;
		StartCoroutine(DestroyOnCollision());
	}

	private IEnumerator DestroyOnCollision()
	{
		yield return new WaitForSeconds(0.2f);
		float timer = 10.0f;
		while(timer >= 0)
		{
			if (physicsSystem.collision.below) this.gameObject.SetActive(false);
			timer -= Time.deltaTime;
			yield return null;
		}
	}
}
