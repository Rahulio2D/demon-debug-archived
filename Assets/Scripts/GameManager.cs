﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
  public string nextLevel;
  void Update()
  {
    if (Input.GetKeyDown(KeyCode.R)) SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();
    if (Input.GetKeyDown(KeyCode.C)) SceneManager.LoadScene(nextLevel);
  }
}
