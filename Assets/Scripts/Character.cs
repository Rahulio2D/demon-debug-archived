﻿using UnityEngine;

[RequireComponent(typeof(BoxCollider2D), typeof(PhysicsSystem))]
public class Character : MonoBehaviour
{
	protected BoxCollider2D boxCollider2D;
	protected Vector2 position;
	protected Vector2 velocity;

	private float gravityRate = 78f;
	private float defaultGravityRate = 0f;

	protected PhysicsSystem physicsSystem;

	protected void Awake()
	{
		position = transform.position;
		velocity = Vector2.zero;
		defaultGravityRate = gravityRate;

		boxCollider2D = GetComponent<BoxCollider2D>();

		physicsSystem = GetComponent<PhysicsSystem>();
		if (!physicsSystem) physicsSystem = gameObject.AddComponent<PhysicsSystem>();
		physicsSystem.Initialise(boxCollider2D, 3, 3, gravityRate);
	}

	protected void Update()
	{
		physicsSystem.MoveWithVelocity(ref position, ref velocity);
		UpdatePosition();
	}

	protected void FixedUpdate()
	{
		physicsSystem.ApplyGravity(ref velocity, 1.5f);
	}

	private void UpdatePosition()
	{
		position += velocity * Time.deltaTime;
		transform.position = position;
	}

	protected bool IsGrounded()
	{
		return physicsSystem.collision.below;
	}

	protected bool GetHorizontalCollision()
	{
		return physicsSystem.collision.right || physicsSystem.collision.left;
	}

	protected void SetGravityRate(float gravityRate)
	{
		this.gravityRate = gravityRate;
		physicsSystem.SetGravityRate(gravityRate);
	}

	protected void SetGravityEnabled(bool enabled)
	{
		if (enabled) SetGravityRate(defaultGravityRate);
		else SetGravityRate(0f);
	}

	protected float GetGravityRate => gravityRate;
}
