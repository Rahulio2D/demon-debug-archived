﻿using System.Collections.Generic;
using UnityEngine;

// This code was created with help from a YouTube tutorial by Sebastian Lague
// https://www.youtube.com/watch?v=MbWK8bCAU2w&t=3s

[RequireComponent(typeof(BoxCollider2D))]
public class PhysicsSystem : MonoBehaviour
{
	private float gravityRate = 1f;

	private Vector2 raycastDirection;
	private Vector2 raycastLength;
	private Vector2 skinWidth;														// Small portion of raycast starting within the box collider
	private Vector2 raySpacing;														// The space between each raycast
	private RaycastHit2D[] verticalRays, horizontalRays;	// Array to store ray data
	private enum RayAxis { Horizontal, Vertical };

	private BoxCollider2D boxCollider2D;
	private Vector2 boxPosition;
	private Vector2 boxSize;

	public Collision collision;
	public struct Collision
	{
		public bool above, below, left, right;  // Collision in said direction?
		public List<GameObject> objects;				// Store reference to all collision objects
	}

	public void Initialise(BoxCollider2D boxCollider2D, int horizontalRayCount, int verticalRayCount, float gravityRate)
	{
		Physics2D.queriesStartInColliders = false;  //Prevent raycasts from detecting the collider of this object
		Physics2D.queriesHitTriggers = false;				//Prevent raycasts from interacting with trigger objects

		this.boxCollider2D = boxCollider2D;
		this.gravityRate = gravityRate;

		verticalRays = new RaycastHit2D[verticalRayCount];
		horizontalRays = new RaycastHit2D[horizontalRayCount];

		raycastLength = Vector2.zero;
		raycastDirection = new Vector2(1, -1);
	}

	public void MoveWithVelocity(ref Vector2 position, ref Vector2 velocity)
	{
		UpdateBoxPositionAndSize(position);
		UpdateRaycastData(velocity);
		ResetCollisionData();

		MoveHorizontal(ref position, ref velocity);
		MoveVertical(ref position, ref velocity);
	}

	private void UpdateBoxPositionAndSize(Vector2 position)
	{
		boxSize = (boxCollider2D.size * transform.localScale) / 2f;
		boxPosition = position + (boxCollider2D.offset * transform.localScale);
	}

	private void UpdateRaycastData(Vector2 velocity)
	{
		raySpacing.x = ((boxSize.x * 2) - skinWidth.x) / (verticalRays.Length - 1);
		raySpacing.y = ((boxSize.y * 2) - skinWidth.y) / (horizontalRays.Length - 1);
		skinWidth = boxSize / 10f;

		// Update the raycast direction if not moving
		if (velocity.x != 0) raycastDirection.x = Mathf.Sign(velocity.x);
		if (velocity.y != 0) raycastDirection.y = Mathf.Sign(velocity.y);

		// Update raycast length if no collisions
		if (!GetHorizontalCollision) raycastLength.x = skinWidth.x + Mathf.Abs(velocity.x) * Time.deltaTime;
		if (!GetVerticalCollision) raycastLength.y = skinWidth.y + Mathf.Abs(velocity.y) * Time.deltaTime;
	}

	private Vector2 GetRayPosition(RayAxis rayAxis, int index)
	{
		Vector2 rayOffset = (boxSize - skinWidth) * raycastDirection;
		Vector2 spaceBetweenRays = raySpacing * index * raycastDirection - (skinWidth / 2f);

		if (rayAxis == RayAxis.Horizontal) 
			return new Vector2(boxPosition.x + rayOffset.x, boxPosition.y - rayOffset.y + spaceBetweenRays.y);
		else
			return new Vector2(boxPosition.x - rayOffset.x + spaceBetweenRays.x, boxPosition.y + rayOffset.y);
	}

	private void MoveHorizontal(ref Vector2 position, ref Vector2 velocity)
	{
		for (int i = 0; i < horizontalRays.Length; i++)
		{
			Vector2 rayPosition = GetRayPosition(RayAxis.Horizontal, i);
			horizontalRays[i] = Physics2D.Raycast(rayPosition, Vector2.right * raycastDirection.x, raycastLength.x);
			Debug.DrawRay(rayPosition, Vector2.right * raycastDirection.x * raycastLength.x, Color.blue);

			if (horizontalRays[i])
			{
				UpdateCollisionData(RayAxis.Horizontal, horizontalRays[i]);
				position.x += (horizontalRays[i].distance - skinWidth.x) * raycastDirection.x;
				raycastLength.x = skinWidth.x;
				velocity.x = 0;
			}
		}
	}

	private void MoveVertical(ref Vector2 position, ref Vector2 velocity)
	{
		for (int i = 0; i < verticalRays.Length; i++)
		{
			Vector2 rayPosition = GetRayPosition(RayAxis.Vertical, i);
			verticalRays[i] = Physics2D.Raycast(rayPosition, Vector2.up * raycastDirection.y, raycastLength.y);
			Debug.DrawRay(rayPosition, Vector2.up * raycastDirection.y * raycastLength.y, Color.blue);

			if (verticalRays[i])
			{
				UpdateCollisionData(RayAxis.Vertical, verticalRays[i]);
				position.y += (verticalRays[i].distance - skinWidth.y) * raycastDirection.y;
				raycastLength.y = skinWidth.y;
				velocity.y = 0;
			}
		}
	}

	private void UpdateCollisionData(RayAxis rayAxis, RaycastHit2D ray)
	{
		if (rayAxis == RayAxis.Horizontal)
		{
			if (raycastDirection.x > 0) collision.right = true;
			else collision.left = true;
		}
		else
		{
			if (raycastDirection.y < 0) collision.below = true;
			else collision.above = true;
		}
		collision.objects.Add(ray.collider.gameObject);
	}

	private void ResetCollisionData()
	{
		collision.above = collision.below = collision.left = collision.right = false;
		if (collision.objects != null) collision.objects.Clear();
		else collision.objects = new List<GameObject>();
	}

	public void ApplyGravity(ref Vector2 velocity, float downMultiplier = 1.0f)
	{
		if (collision.below) return;
		float gravity = velocity.y > 0 ? gravityRate : gravityRate * downMultiplier;
		velocity.y -= gravity * Time.fixedDeltaTime;
	}

	public bool HasCollided()
	{
		return collision.above || collision.below || collision.left || collision.right;
	}

	public bool CheckCollision(string collisionTag)
	{
		if (HasCollided())
		{
			for (int i = 0; i < collision.objects.Count; i++)
			{
				if (collision.objects[i].CompareTag(collisionTag)) return true;
			}
		}
		return false;
	}

	public List<GameObject> GetCollisionObjectsByTag(string collisionTag)
	{
		List<GameObject> objects = new List<GameObject>();
		foreach (GameObject collisionObject in collision.objects)
		{
			if (collisionObject.CompareTag(collisionTag)) objects.Add(collisionObject);
		}
		return objects;
	}

	public Vector2 GetDirection()
	{
		return raycastDirection;
	}

	public void SetGravityRate(float gravityRate)
	{
		this.gravityRate = gravityRate;
	}

	public bool GetHorizontalCollision => collision.left || collision.right;
	public bool GetVerticalCollision => collision.above || collision.below;
}
